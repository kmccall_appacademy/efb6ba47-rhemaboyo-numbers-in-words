class Fixnum
  def singles
    {
      1 => 'one',   2 => 'two',
      3 => 'three', 4 => 'four',
      5 => 'five',  6 => 'six',
      7 => 'seven', 8 => 'eight',
      9 => 'nine',  0 => 'zero'
    }
  end

  def teens
    {
      10 => 'ten',      11 => 'eleven',
      12 => 'twelve',   13 => 'thirteen',
      14 => 'fourteen', 15 => 'fifteen',
      16 => 'sixteen',  17 => 'seventeen',
      18 => 'eighteen', 19 => 'nineteen'
    }
  end

  def tens
    {
      2 => 'twenty', 3 => 'thirty',
      4 => 'forty',  5 => 'fifty',
      6 => 'sixty',  7 => 'seventy',
      8 => 'eighty', 9 => 'ninety',
    }
  end

  def greaters(num, prefix = nil)
    if num == 10
      return "#{tens[self / num]} #{(self % num).in_words}".chomp(' zero')
    end
    "#{(self / num).in_words} #{prefix} #{(self % num).in_words}".chomp(' zero')
  end

  def in_words
    return singles[self] if self < 10
    return teens[self] if self < 20
    return greaters(10) if self < 100
    return greaters(100, 'hundred') if self < 1000
    return greaters(1000, 'thousand') if self < 1_000_000
    return greaters(1_000_000, 'million') if self < 1_000_000_000
    return greaters(1_000_000_000, 'billion') if self < 1_000_000_000_000
    greaters(1_000_000_000_000, 'trillion')
  end

end
